"""
Query the GiantBomb API for games that release in a certain range of dates.  This comes back imperfectly, so we have
to manually verify the dates and platforms we care about.
"""
import requests
import datetime
import sys
import time
import re
import unicodedata


GB_API_KEY = ''
month_to_start_year = 10
# Get the dates we care about.  Right now it's October 1st of *last* year to September 30th of *this* year.
# Car manufacturer model years, basically.  Can change on a whim.  Change month_to_start_year to do this.
request_headers = {
    "User-Agent": "SET THIS"
}

gb_date_format = '%Y-%m-%d %H:%M:%S'


now = datetime.datetime.now()
current_year = now.year

# October 1st, prior year.
year_start = datetime.datetime(current_year-1, month_to_start_year, 1)

year_end = year_start - datetime.timedelta(1)
year_end = year_end.replace(year=current_year)

print("Year Start:", year_start)
print("Year End:", year_end)

year_start_string = year_start.strftime(gb_date_format)
year_end_string = year_end.strftime(gb_date_format)

def convert_to_metacritic_slug(text):
    slug = unicodedata.normalize('NFKD', text)
    slug = slug.encode('ascii', 'ignore').lower()
    slug = re.sub(r'[^a-z0-9]+', '-', str(slug)).strip('-')
    slug = re.sub(r'[-]+', '-', str(slug))
    return slug[2:]


def get_gb_pages(url):
    if 'format=json' not in url:
        return False

    return_data = []
    api_response = requests.get(url, headers=request_headers).json()
    print(api_response['number_of_total_results'])
    time.sleep(3)
    while api_response['offset'] + api_response['number_of_page_results'] < api_response['number_of_total_results']:
        print("Loop")
        for response_object in api_response['results']:
            yield response_object

        offset = api_response['offset'] + api_response['number_of_page_results']
        new_url = "{}&offset={}".format(url, offset)
        print(new_url)
        api_response = requests.get(new_url, headers=request_headers).json()
        time.sleep(1)


def get_opencritic_id(game_name):
    # Returns the ID of the first result.
    time.sleep(1)
    f = requests.get('http://opencritic.com/api/site/search?criteria={}'.format(game_name), headers=request_headers)
    f = f.json()
    return f[0]['id']


def get_opencritic_score(game_id):
    exclude_these_types = [28, 30, 31, 32, 33, 34]  # This comes from Opencritic themselves.
    f = requests.get('http://opencritic.com/api/game?id={}'.format(game_id))
    opencritic_data = f.json()
    review_scores = []
    if f.status_code == 200:
        for review in opencritic_data['Reviews']:
            if review['ScoreFormatId'] not in exclude_these_types:
                review_scores.append(review['score'])

        if review_scores:
            return round(sum(review_scores)/len(review_scores), 2)
    return None


gb_api_games = get_gb_pages('https://www.giantbomb.com/api/games/?api_key={}&limit=100'
                            '&filter=original_release_date:{}|{}'
                            '&field_list=id,name,original_release_date,platforms,date_last_updated'
                            '&sort=date_last_updated:desc&format=json'.format(GB_API_KEY,
                                                                              year_start_string,
                                                                              year_end_string))

actually_released_games = []

for game in gb_api_games:
    release_platform_ids = []
    if game['platforms']:
        for platform in game['platforms']:
            release_platform_ids.append(platform['id'])

    if not game['original_release_date']:
        continue

    if 94 not in release_platform_ids:
        continue

    date_updated = datetime.datetime.strptime(game['date_last_updated'], gb_date_format)
    if year_start > date_updated:
        break

    release_date = datetime.datetime.strptime(game['original_release_date'], gb_date_format)
    if year_start < release_date < year_end:
        actually_released_games.append(game)

output_data = []

print(len(actually_released_games), 'released in the desired timeframe.  Getting their scores.')

i = 0
for game in actually_released_games:
    oc_id = get_opencritic_id(game['name'])
    oc_score = get_opencritic_score(oc_id)
    print(i, len(actually_released_games), game['name'], oc_id, oc_score)
    output_data.append({'name': game['name'], 'score': oc_score, 'release_date': game['original_release_date']})
    i += 1

print(output_data)